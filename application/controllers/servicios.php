<?php

class Servicios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("servicios_model");
        if(!$this->user->loggedin) {
            redirect(site_url("login"));
        }
    }


    public function index()
    {
        // Pagination
        $config['base_url'] = site_url("person/index/");
        $data['centroCostos'] = $this->servicios_model->getQuery("centrocostos");
        $data['clientes'] = $this->servicios_model->getQuery("clientes");
        $data['insumos'] = $this->servicios_model->getQuery("insumos");
        $data['message'] = "hola";
        $this->template->loadContent("person/index.php", $data);
    }

    function getJson(){
         $data['insumos'] = $this->servicios_model->jsonCentroCostos();
         $this -> output -> set_content_type('application/json');
         $this -> output -> set_output(json_encode($data, JSON_FORCE_OBJECT));
    }

    function getWhere($idinsumo){
        $data['insumos'] = $this->servicios_model->jsonSelectWhere($idinsumo);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data, JSON_FORCE_OBJECT));
    }

    function getMaxidReq(){
        $data = $this->servicios_model->jsonGetMax();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data, JSON_FORCE_OBJECT));
    }

    public function saveJson() {
        $data = json_decode(stripslashes($_POST['data']));
        $array = array();
        $array = $data;
        foreach($array as $key =>$value){
          $student = array(
                            'clave' => $array[$key]->clave,
                            'descripcion1' => $array[$key]->descripcion1,
                            'unidad1' => $array[$key]->unidad1,
                            'cantidad' => $array[$key]->cantidad,
                            'fecha' => $array[$key]->fecha,
                            'comentarios' => $array[$key]->comentarios,
                            );

        $this->db->insert('deta_requisiciones', $student);

        }
    }

    public function getConsecutivo($idCentroCostos){
      $data = $this->servicios_model->jsonSelectCountCc($idCentroCostos);
      $this->output->set_content_type('application/json');
      $this->output->set_output(json_encode($data, JSON_FORCE_OBJECT));
    }

/* ********************************* FUNCIONES GUARDAR *************************************** */
    function saveReq() {
      $my_date = date("Y-m-d", time());

        if ($this->input->post()) {
            $requiArr = array(
                    'claveCompuesta' => $this->input->post('claveCompuesta'),
                    'idCliente' => $this->input->post('idCliente'),
                    'idCentroCostos' => $this->input->post('idCentroCostos'),
                    'idSolicita' => $this->input->post('idSolicita'),
                    'obra' => $this->input->post('obra'),
                    'fecha' => $my_date);
            echo $this->servicios_model->transaction_requisiciones($requiArr);
        }
    }

    function table_data() {
        if ($this->input->post()) {
            $table_data = $this->input->post('table');
            $this->servicios_model->insert_table_data($table_data);
        }
    }


}
