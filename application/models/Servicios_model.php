<?php

class Servicios_model extends CI_Model
{
    /*
        Seleccionar Todos los elementos de una tabla
    */
    public function get_data()
    {
        return $this->db->select("*")
        ->get("servicios");
    }


    public function getQuery($table){
       /* $sql = "SELECT * FROM centrocostos WHERE id = ? AND status = ? AND author = ?";
        $this->db->query($sql, array(3, 'live', 'Rick'));*/
        return $this->db->query("Select * from ". $table);
    }


    public function jsonCentroCostos(){

        $q = $this->db->query("Select * from insumos limit 5");

        if ($q -> num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function jsonSelectWhere($idinsumo){
        $q = $this->db->query("Select descripcion, unidad from insumos where idInsumo = ?", $idinsumo);
        if ($q -> num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function jsonGetMax(){
        $q = $this->db->query("CALL getMax()");
        if ($q -> num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

  public function jsonSelectCountCc($idCentroCostos){
      $q = $this->db->query("Select COUNT(idCentroCostos) + 1 AS CountCc FROM requisiciones WHERE idCentroCostos = ?", $idCentroCostos);
      if ($q -> num_rows() > 0) {
          foreach ($q->result() as $row) {
              $data[] = $row;
          }
          return $data;
      }
  }
/*
    function form_insert($data){
        // Inserting in Table(students) of Database(college)
        $this->db->insert('requisiciones', $data);
    }
*/
    function insert_table_data($table_data){
        $this->db->insert_batch('deta_requisiciones', $table_data);
    }

    function transaction_requisiciones($requiArr){
        $this->db->trans_begin();
        $this->db->insert('requisiciones', $requiArr);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return "No Insertado";
          }
        else{
          $this->db->trans_commit();
          return "Insertado";
      }
    }



}
?>
