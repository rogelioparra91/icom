<link href="<?php echo base_url();?>styles/jquery.bdt.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>styles/style.css" rel="stylesheet" type="text/css">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?>
    </div>
    <div class="db-header-extra"><input type="button" class="btn btn-primary btn-sm" value="<?php echo lang("ctn_14") ?>" data-toggle="modal" data-target="#memberModal" />
    </div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active"><?php echo lang("ctn_15") ?></li>
</ol>

<p><?php echo lang("ctn_51") ?></p>
<hr>
            <table class="table table-bordered" id="bootstrap-table">
                <thead>
               <tr class="table-header">
                    <th>#Clave</th>
                    <th>Descripción</th>
                    <th>Cuenta</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($groups->result() as $r) : ?>
                    <tr>
                        <td><?php echo $r->clave ?></td>
                        <td><?php echo $r->descripcion ?></td>
                        <td><?php echo $r->cuenta ?></td>
                        <td>
                  <?php if($this->user->loggedin && $this->user->info->user_level == 5) : ?>
                          <a href="<?php echo site_url("admin/edit_member/" . $r->clave) ?>" class="btn btn-warning btn-xs"><?php echo lang("ctn_55") ?>
                          </a>
                          <a href="<?php echo site_url("admin/delete_member/" . $r->clave . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('<?php echo lang("ctn_86") ?>')" class="btn btn-danger btn-xs"><?php echo lang("ctn_57") ?>
                          </a>
                  <?php endif; ?>       
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>


<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang("ctn_14") ?></h4>
      </div>
      <div class="modal-body">
      <?php echo form_open(site_url("admin/add_group_pro"), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_18") ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="email-in" name="name">
                    </div>
            </div>
            <div class="form-group">

                        <label for="username-in" class="col-md-3 label-heading"><?php echo lang("ctn_19") ?></label>
                        <div class="col-md-9">
                            <input type="checkbox" name="default_group" value="1">
                            <span class="help-block"><?php echo lang("ctn_59") ?></span>
                        </div>
            </div>
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_61") ?>" />
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>bootstrap/js/jquery.sortelements.js"></script>
<script src="<?php echo base_url();?>bootstrap/js/jquery.bdt.min.js"></script>



<script>
    $(document).ready( function () {
        $('#bootstrap-table').bdt({
            showSearchForm: 1,
            showEntriesPerPageField: 0
        });
    });
</script>