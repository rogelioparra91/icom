<script src="<?php echo base_url();?>bootstrap/js/datatables.js"></script>
<div class="white-area-content">
    <div id="message" class="hidden alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning! </strong><?php echo $message ?>
    </div>
</div>
    <div class="panel panel-danger">
        <div class="panel-heading"><span class="glyphicon glyphicon-copy"></span> REQUISICIÓN DE MATERIAL EN OBRA
        <div class="db-header-extra"> <strong><div id="lblIdRequisiciones"></div> </strong></div>
        </div>
      <div class="panel-body">
        <form id="formRequisiciones" action="<?php echo base_url(); ?>servicios/saveReq" method="post">
            <div class="row">
                <input  type="hidden" id="claveCompuesta" name="claveCompuesta">
                    <div class="col-sm-6">
                    <label for="email">Obra:</label>
                        <input type="text" name="obra" class="form-control input-sm input-sm" placeholder="Obra">
                    </div>
                <div class="col-sm-2">
                 <label for="email">Solicita:</label>
                 <input type="hidden" name="idSolicita" value="<?php echo $this->user->info->ID ?>">
                    <input type="text" class="form-control input-sm" placeholder="Solicita" value="<?php echo $this->user->info->first_name .' '. $this->user->info->last_name?>" disabled>
                </div>
                <div class="col-sm-3">
                 <label for="email">Fecha:</label></br>
                    <strong><?php  $mydate = getdate(date("U"));
                            echo "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
                        ?></strong>
                </div>
                </div>
        </br>
           <div class="row">

                <div class="col-sm-2">
                 <label for="email">IdCliente:</label>
                    <input type="text" id="idCliente" class="form-control input-sm" placeholder="Clave del Cliente" disabled>
                </div>
                <div class="col-sm-2">
                 <label for="email">Cliente:</label>
                    <select select id="cbClientes" name="idCliente" class="form-control input-sm">
                        <?php foreach($clientes->result() as $r) : ?>
                            <option value="<?php echo $r->idCliente ?>"><?php echo $r->nombre ?></option >
                        <?php endforeach; ?>
                    </select>
                </div>
                    <div class="col-sm-2">
                    <label for="email">Clave:</label>
                        <input type="text" id="idCentroCostos" class="form-control input-sm" placeholder="Centro de Costos" disabled >
                    </div>
                    <div class="col-sm-5">
                        <label for="email">Centro Costos:</label>
                        <select id="cbCentroCostos" name="idCentroCostos" class="form-control input-sm">
                            <?php foreach($centroCostos->result() as $r) : ?>
                                <option value="<?php echo $r->clave ?>"><?php echo $r->descripcion ?></option >
                            <?php endforeach; ?>
                        </select>
                    </div>
        
            </div>
        </form>
        </br><legend>Busqueda de Insumos</legend>
            <div class="row">
                <div class="col-sm-2 div-control-inline">
                 <label for="email">Clave:</label>
                     <input type="text" id="clave1" name="jclave[]" required="" class="btnBuscar form-control input-sm form-control-inline" placeholder="Clave del Insumo" maxlength="1" size="1">
                </div>
                <div class="col-sm-4">
                 <label for="email">Descripcion:</label>
                    <input type="text" id="descripcion1" name="jdescrip[]" class="form-control input-sm" placeholder="Descripcion del Insumo" maxlength="40" size="40" disable>
                </div>
                <div class="col-sm-2 div-control-inline">
                 <label for="email">Unidad:</label>
                    <input type="text" id="unidad1" name="junidad[]" class="form-control input-sm" placeholder="Unidad de Medida" disabled maxlength="3" size="3">
                </div>
                <div class="col-sm-2 div-control-inline">
                 <label for="email">Cantidad:</label>
                    <input type="text" id ="cantidad1" name="jcantidad[]" class="form-control input-sm" maxlength="4" size="4">
                </div>
                <div class="col-sm-2">
                 <label for="email">fecha:</label>
                    <input type="text" id="fecha" name="jfecha[]" required="" class="form-control input-sm datepicker" placeholder="Date" maxlength=5 size=5>
                </div>

            </div>  </br>
            <div class="row">
                <div class="col-sm-10">
                     <input type="text" id="comentarios" name="jclave[]" required="" class="form-control" placeholder="Ingrese comentarios para esta partida">
                </div>
                <div class="col-sm-1">
                    <button type="button" id="btnAgregar" class="btn btn-warning"><i class="glyphicon glyphicon-plus">Agregar</i></button>
                </div>

            </div>
      </div>
    </div>

    <div class="panel panel-info">
      <div class="panel-heading">Panel with panel-info class</div>
      <div class="panel-body">
        <form id="formDetalles" action="<?php echo base_url(); ?>servicios/table_data" method="post">
          <table id="table" class="table table-bordered">
                <tbody>
                  <th>Clave</th>
                  <th class="hidde"></th>
                  <th>Descripción</th>
                  <th>Unidad</th>
                  <th>Cantidad</th>
                  <th>Fecha</th>
                  <th>Comentarios</th>
                </tbody>
                 <tr>

                </tr>
          </table
      </form>              
          <div class="col-sm-5">
              <input id="btnAccion" type="button" class="form-control" value="Click Me!"/>
          </div>
      </div>
    </div>

<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div id="test-list" class="row">
            <div class="list-center col-md-12">
                <div class="well text-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-search"></span>
                            <input type="text" name="SearchDualList" class="form-control search" placeholder="Busqueda" />
                        </div>
                    </div>

                </div>
                    <hr>
                    <ul id="insumos" class="list list-group">
                        <?php foreach($insumos->result() as $r) : ?>
                            <li class="list-group-item" data-id="<?php echo $r->idInsumo ?>" data-unidad="<?php echo $r->unidad ?>" >
                                <p class="name"><?php echo $r->idInsumo ?> -- <?php echo $r->descripcion ?> -- <?php echo $r->unidad ?></p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <ul class="pagination"></ul>
                </div>
            </div>
        </div>

      </div>

    </div>
  </div>
</div>
