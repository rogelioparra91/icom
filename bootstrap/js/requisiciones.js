var myObject = [];
var numCc = [];
//var myObject = {item: [{ }]};
$(document).ready(function() {

	$("body").on('click', '.btn-add-more', function(e) {
		e.preventDefault();
		var $sr = ($(".jdr1").length + 1);
		var rowid = Math.random();
		var $html = '<tr class="jdr1" id="' + rowid + '">' + '<td><span class="btn btn-sm btn-default">' + $sr + '</span><input type="hidden" name="count[]" value="' + Math.floor((Math.random() * 10000) + 1) + '"></td>' + '<td><input type="text" id="clave' + $sr + '" name="jclave[]" required="" class="form-control input-sm" placeholder="Clave del Insumo" maxlength="7" size="7"></td>' + '<td><input type="text" name="jtype[]" placeholder="Travel by" class="form-control input-sm"></td>' + '<td><input type="text" name="jpassanger[]" placeholder="Paasenger count" class="form-control input-sm"></td>' + '<td><input type="text" name="jfrom[]" placeholder="Depart from" class="form-control input-sm"></td>' + '<td><input type="text" name="jdate[]" placeholder="Date" class="form-control input-sm datepicker"></td>' + '<td><input type="text" name="jamount[]" placeholder="Amount" class="form-control input-sm"></td>' + '</tr>';
		$("#table-details").append($html);
	});

	$("body").on('click', '.btn-remove-detail-row', function(e) {
		e.preventDefault();
		if ($("#table-details tr:last-child").attr('id') != 'row1') {
			$("#table-details tr:last-child").remove();
		}
	});

	$("body").on('focus', ' .datepicker', function() {
		$(this).datepicker({
			dateFormat : "yy-mm-dd" });
	});

	$("#frm_submit").on('submit', function(e) {
		e.preventDefault();
		$.ajax({
			url : '<?php echo base_url() ?>welcome/batchInsert',
			type : 'POST',
			data : $("#frm_submit").serialize()
		}).always(function(response) {
			var r = (response.trim());
			if (r == 1) {
				$(".alert-success").show();
			} else {
				$(".alert-danger").show();
			}
		});
	});

	$(".datepicker").datepicker({
		dateFormat : 'dd/mm/yy'
	});

	$("#tecnico").autocomplete({
		source : "<?php echo base_url(); ?>index.php/os/autoCompleteUsuario",
		minLength : 1,
		select : function(event, ui) {

			$("#usuarios_id").val(ui.item.id);
		}
	});

	var insumosList = new List('test-list', {
		valueNames : ['name'],
		page : 5,
		plugins : [ListPagination({})]
	});

	$(".btnBuscar").click(function() {
		$('#memberModal').modal();
		callInsumos.loadData();
	var value = $(this).attr('id');
		var text = "";
		switch (value) {
			  case "clave1":
			//	text = "Primera Partida";
				break;
			  case "clave2":
				text = "Segunda Partida";
				break;
			default:
				text = "Looking forward to the Weekend";
		}
	});

/* ************************************* ABRIR MODAL ************************************************** */
$('#insumos li').click(function(){
	$("#clave1").val($(this).data("id"));
	myFunction($(this).data("id"));
	$('#memberModal').modal('toggle');
})
/* ************************************* BOTON AÑADIR PARTIDA **************************************** */
$('#btnAgregar').click(function(){
	var $sr = ($(".jdr1").length + 1);
	getLastInsert();
	$('#table tr:last').after('<tr class = "jdr1">'+
	'<td><input class="form-control" name="table['+ $sr +'][clave]" value="'+ $("#clave1").val() +'"></td>'+
	'<td><input class="requisicionTxt" name="table['+ $sr +'][idRequisiciones]"></td>'+
	'<td><input class="form-control" value="'+ $("#descripcion1").val() +'"></td>'+
	'<td><input class="form-control" value="'+ $("#unidad1").val() +'" disable></td>'+
	'<td><input class="form-control" name="table['+ $sr +'][cantidad]" value="'+ $("#cantidad1").val() +'"></td>'+
	'<td><input class="form-control" name="table['+ $sr +'][fecha]" value="'+ dateConvert($("#fecha").val()) +'"></td>'+
	'<td><input class="form-control" name="table['+ $sr +'][comentarios]" value="'+ $("#comentarios").val() +'"></td>'+
	'</tr>');
	myObject.push(
    {clave: $("#clave1").val(), descripcion1: $("#descripcion1").val() , unidad1: $("#unidad1").val(), cantidad : $("#cantidad1").val(), fecha : $("#fecha").val(), comentarios : $("#comentarios").val()}
	);
});
/* ************************************* FIN AÑADIR  ************************************************** */

/*	**** GuardarRequisicion **** */
$('#btnAccion').click(function (){
//	document.forms["formRequisiciones"].submit() === undefined)
		$.post($("#formRequisiciones").attr("action"), $("#formRequisiciones").serialize(),
			function() {
				$.post($("#formDetalles").attr("action"), $("#formDetalles").serialize(),
					function() {
						//CallBack
						$('#formRequisiciones')[0].reset();
						$('#formDetalles')[0].reset();
						$("#table").empty();
						$("#message").removeClass("hidden");
					//	getMaxidReq(1);
					});
			});
});
/*	**** FIN: GuardarRequisicion **** */
/* ****************** ComboBox ******************* */
	$('#cbClientes').on('change', function() {
		 $("#idCliente").val(this.value);
	});

	$('#cbCentroCostos').on('change', function() {
		var jsonResponse = JSON.parse(getConsecutivo(this.value));
		 $("#idCentroCostos").val(this.value);
		 $("#lblIdRequisiciones").empty();
 	 	 $( "#lblIdRequisiciones" ).append( "REQ-"+ this.value + "-" + jsonResponse[0]['CountCc'] +"");
 	 	 $( "#claveCompuesta" ).val( "REQ-"+ this.value + "-" + jsonResponse[0]['CountCc'] +"");
	});
/* ****************** FIN: ComboBox ******************* */
/*******************/
	/*FIN JQUERY*/
});

/************************* FUNCIONES **********************************/
function dateConvert(date){
	var d=new Date(date.split("/").reverse().join("-"));
	var dd = d.getDate()+1;
	var mm = d.getMonth()+1;
	var yy = d.getFullYear();
	return yy + "/" + mm + "/" + dd;CountCc
}

/*
Funcion: busca un insumo y consulta su descripcion y su unidad de medida
*/
function myFunction(id) {
	var url;
	var insumo = [];
	var length;

	url = 'http://localhost/intranet/servicios/getWhere/' + id;

	 $.getJSON(url, function (data) {
		 $.each(data, function (i) {
			 insumo.push(data[i]);
		 })
			$("#descripcion1").val(insumo[0][0]['descripcion']);
			$("#unidad1").val(insumo[0][0]['unidad']);
			$('#cantidad1').focus();
	 });
}

function getLastInsert() {
	 $.getJSON('getMaxidReq', function (data) {
					$(".requisicionTxt").val( (parseInt(data[0]['idRequisiciones'])/*+ 1*/) );
	 });
}

getConsecutivo = function (idCentroC) {
    var result = $.ajax({
        type: "POST",
        url: 'getConsecutivo/'+ idCentroC,
       param: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
       async: false,
        success: function (data) {
      //    return data[0]['CountCc'];
      }
    }) .responseText ;
    return  result;
}

$.fn.liveSearch = function(config) {
	var table = $(config.table), children, searchString;
	$(this).keyup(function(){
		//Get all table columns
		children = table.find('td');
		searchString = $(this).val().toLowerCase();
		//Hide all rows except the for table header
		table.find('tr:gt(0)').hide();
		//Loop through all table columns
		children.each(function(index, child){
			//If search string matches table column
			if(child.innerHTML.toLowerCase().indexOf(searchString) != -1) {
				//Show table row
				$(child).closest('tr').show();
			};
		});
	});
};


function print_r(printthis, returnoutput) {
	var output = '';

	if ($.isArray(printthis) || typeof (printthis) == 'object') {
		for (var i in printthis) {
			output += i + ' : ' + print_r(printthis[i], true) + '\n';
		}
	} else {
		output += printthis;
	}
	if (returnoutput && returnoutput == true) {
		return output;
	} else {
		alert(output);
	}
}
