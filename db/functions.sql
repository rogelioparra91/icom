SELECT MAX(idRequisiciones) AS idRequisiciones FROM requisiciones;

DELIMITER //
CREATE PROCEDURE getMax()
 BEGIN
     SELECT MAX(idRequisiciones + 1)  AS idRequisiciones  FROM requisiciones ;
 END //
DELIMITER ;

call getMax() ;

DELIMITER //

SELECT COUNT(idCentroCostos) + 1 AS CountCc FROM requisiciones WHERE idCentroCostos = 'GP001';