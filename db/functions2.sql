SELECT MAX(idRequisiciones) AS idRequisiciones FROM requisiciones;
DROP PROCEDURE IF EXISTS getMax;

DELIMITER //
CREATE PROCEDURE getMax()
 BEGIN
     SELECT (Auto_Increment) AS idRequisiciones FROM INFORMATION_SCHEMA.TABLES WHERE Table_name = 'requisiciones';
 END //
DELIMITER ;

call getMax() ;

DELIMITER //

SELECT COUNT(idCentroCostos) + 1 AS CountCc FROM requisiciones WHERE idCentroCostos = 'GP001';


INSERT INTO requisiciones (idRequisiciones, idCliente, claveCompuesta, idCentroCostos, idSolicita, obra) 
	VALUES ('3', 'BAR', 'REQ-ADMII-2', 'AL001', '1', 'Obra 1');
    
INSERT INTO deta_requisiciones (idDetalle, idRequisiciones, clave, cantidad, fecha, comentarios) 
	VALUES ('4', '3', '10101009', '10', '2016-15-06', '5comentarios');

SELECT * FROM iicomdb.requisiciones;
