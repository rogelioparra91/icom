create database iicomdb;
use iicomdb;

  CREATE TABLE IF NOT EXISTS ip_block (
  ID int(11) NOT NULL AUTO_INCREMENT,
  IP varchar(500) NOT NULL,
  `timestamp` int(11) NOT NULL,
  reason varchar(1000) NOT NULL,
  PRIMARY KEY (ID));

  CREATE TABLE IF NOT EXISTS home_stats (
  ID int(11) NOT NULL AUTO_INCREMENT,
  google_members int(11) NOT NULL,
  facebook_members int(11) NOT NULL,
  twitter_members int(11) NOT NULL,
  total_members int(11) NOT NULL,
  new_members int(11) NOT NULL,
  active_today int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  PRIMARY KEY (ID));

CREATE TABLE IF NOT EXISTS users (
  ID int(11) NOT NULL AUTO_INCREMENT,
  email varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  password varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  token varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  user_level int(11) NOT NULL DEFAULT '0',
  IP varchar(500) CHARACTER SET utf8 NOT NULL DEFAULT '',
  username varchar(25) CHARACTER SET utf8 NOT NULL,
  first_name varchar(25) CHARACTER SET utf8 NOT NULL,
  last_name varchar(25) CHARACTER SET utf8 NOT NULL,
  avatar varchar(1000) NOT NULL DEFAULT 'default.png',
  joined int(11) NOT NULL,
  joined_date varchar(10) NOT NULL,
  online_timestamp int(11) NOT NULL,
  oauth_provider varchar(40) NOT NULL,
  oauth_id varchar(1000) NOT NULL,
  oauth_token varchar(1500) NOT NULL,
  oauth_secret varchar(500) NOT NULL,
  email_notification int(11) NOT NULL DEFAULT '1',
  aboutme varchar(1000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (ID));


CREATE TABLE nivelAcceso(
	idNivelAcceso integer auto_increment,
	descripcion VARCHAR(20) NOT NULL,
PRIMARY KEY(idNivelAcceso));

	-- Relacionando la tabla usuario_nivelAcceso
	ALTER TABLE users
	  ADD CONSTRAINT nivelAcces_usuario_FK FOREIGN KEY (user_level) REFERENCES nivelAcceso (idNivelAcceso)
		ON DELETE CASCADE ON UPDATE CASCADE, ADD INDEX nivelAcces_usuario_IDX (user_level ASC);

create table centroCostos (
    clave varchar (20) NOT NULL,
    descripcion varchar (100) NOT NULL,
	cuenta int(11) NOT NULL,
primary key(clave))DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS user_ccost_users (
   id  int(11) NOT NULL AUTO_INCREMENT,
   idCentroCostos varchar (20) NOT NULL,
   idUsuario  int(11) NOT NULL,
  PRIMARY KEY (ID))DEFAULT CHARSET=utf8;

	-- Relacionando la tabla usuario_centroCostos
	ALTER TABLE user_ccost_users
	  ADD CONSTRAINT centroCostso_usuario_FK FOREIGN KEY (idUsuario) REFERENCES users (id)
		ON DELETE CASCADE ON UPDATE CASCADE, ADD INDEX centroCostso_usuario_IDX (idUsuario ASC);

	-- Relacionando la tabla usuario_centroCostos
	ALTER TABLE user_ccost_users
	  ADD CONSTRAINT centroCostso_CentroCostos_FK FOREIGN KEY (idCentroCostos) REFERENCES centroCostos (clave)
		ON DELETE CASCADE ON UPDATE CASCADE, ADD INDEX centroCostso_CentroCostos_IDX (idCentroCostos ASC);

create table unidadMedida (
    unidad varchar (20) NOT NULL,
    descunidad varchar (100) NOT NULL,
primary key(unidad));

create table insumos (
    idInsumo varchar (20) NOT NULL,
    descripcion varchar (100) NOT NULL,
    unidad varchar (20) NOT NULL,
primary key(idInsumo));

	ALTER TABLE insumos
	  ADD CONSTRAINT insumos_Producto_FK FOREIGN KEY (unidad) REFERENCES unidadMedida (unidad)
		ON DELETE CASCADE ON UPDATE CASCADE, ADD INDEX insumos_Producto_IDX (unidad ASC);

create table requisiciones(
  idRequisiciones integer auto_increment,
  idCliente integer not null,
  idCentroCostos integer not null,
  idSolicita integer not null,
  obra varchar(200),
  fecha date,
primary key(idRequisiciones));

create table deta_requisiciones(
  idDetalle integer auto_increment,
  idRequisiciones integer,
  clave integer,
  descripcion varchar(200),
  unidad char (4),
  cantidad integer,
  fecha date,
  comentarios text,
PRIMARY KEY (idDetalle, idRequisiciones));


create table requisiciones(
  idRequisiciones integer auto_increment,
  idCliente char (6) not null,
  claveCompuesta varchar (20) not null,
  idCentroCostos char (6) not null,
  idSolicita integer not null,
  obra varchar(200) not null,
  fecha date,
primary key(idRequisiciones));

create table deta_requisiciones(
  idDetalle integer auto_increment,
  idRequisiciones integer not null,
  clave integer not null,
  cantidad integer not null,
  fecha date not null,
  comentarios text,
PRIMARY KEY (idDetalle, idRequisiciones));
    -- alter table users drop foreign key nivelAcces_usuario_FK
   /*
		ALTER TABLE users DROP INDEX nivelAcces_usuario_IDX
		ALTER TABLE users DROP INDEX nivelAcces_usuario_IDX
		ALTER TABLE user_ccost_users DROP INDEX centroCostso_usuario_IDX
		ALTER TABLE nivelAcceso DROP COLUMN `default`;
    */

    -- Copia de talba
    /*

    INSERT INTO insumos SELECT CVE_PROD, DESC_PROD, UNI_MED FROM producto WHERE CVE_PROD is not null;

    */

    create table clavesPrimarias (
        idClaves int not null AUTO_INCREMENT,
        idRequisiciones integer NOT NULL,
        idCentroCostos char (5) NOT NULL,
        consecutivo integer NOT NULL,
    primary key(idClaves));
