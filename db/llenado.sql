
INSERT INTO nivelacceso (idNivelAcceso,descripcion) VALUES (0,'User');
INSERT INTO nivelacceso (descripcion) VALUES ('Capturista');
INSERT INTO nivelacceso (descripcion) VALUES ('Compras');
INSERT INTO nivelacceso (descripcion) VALUES ('Gerente');
INSERT INTO nivelacceso (descripcion) VALUES ('Admin General');
INSERT INTO nivelacceso (descripcion) VALUES ('Banned');

INSERT INTO users (ID,email,password,token,user_level,IP,username,first_name,last_name,avatar,joined,joined_date,online_timestamp,oauth_provider,oauth_id,oauth_token,oauth_secret,email_notification,aboutme) VALUES (1,'fperez@iicom.mx','$2a$12$tQGy/0/Y/HFgyuAGJbA0ouSdQAMPdEPrKB4lfRBCZrb5uOBT15di.','83832ac3646ee037f4e418740fe44bda',5,'127.0.0.1','HeyZues','Rogelio','Parra','default.png',1465583119,'6-2016',1465826368,'','','','',1,'');
INSERT INTO users (ID,email,password,token,user_level,IP,username,first_name,last_name,avatar,joined,joined_date,online_timestamp,oauth_provider,oauth_id,oauth_token,oauth_secret,email_notification,aboutme) VALUES (2,'mmacias@iicom.mx','$2a$12$pL57tg3rwLcUWf7gO7GwWuhpH0fx6FQpzBwSZC92iQ/CRxzWfXSie','f7c8df5be58ee369ad70ad695b17e2c7',3,'127.0.0.1','mmacias','Miguel','Macias','default.png',1465589171,'6-2016',1465825905,'','','','',1,'');

INSERT INTO home_stats (ID, google_members, facebook_members, twitter_members, total_members, new_members, active_today, timestamp) VALUES
(1, 0, 0, 0, 0, 0, 0, 0);
COMMIT;

INSERT INTO `site_settings` (`ID`, `site_name`, `site_desc`, `upload_path`, `upload_path_relative`, `site_email`, `site_logo`, `register`, `disable_captcha`, `date_format`, `avatar_upload`, `file_types`, `twitter_consumer_key`, `twitter_consumer_secret`, `disable_social_login`, `facebook_app_id`, `facebook_app_secret`, `google_client_id`, `google_client_secret`, `file_size`) VALUES
(1, 'ProLogin', 'Welcome to ProLogin', '/home/www/public_html/uploads', 'uploads', 'test@test.com', 'logo.png', 0, 0, 'd/m/Y', 1, 'gif|png|jpg|jpeg', '', '', 1, '', '', '', '', 1028);

INSERT INTO user_ccost_users (idCentroCostos, idUsuario) VALUES ('4', '2');
INSERT INTO user_ccost_users (idCentroCostos, idUsuario) VALUES ('3', '2');
INSERT INTO user_ccost_users (idCentroCostos, idUsuario) VALUES ('2', '2');

INSERT INTO unidadmedida (unidad, descunidad) VALUES ('unidad', 'descunidad');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('COS', 'COSTAL');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('CUB', 'CUBETA');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('DIA', 'DIAS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('HRS', 'HORAS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('JGO', 'JUEGO');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('KGS', 'KILOGRAMOS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('LOT', 'LOTE');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('LTS', 'LITROS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('MES', 'MESES');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('MIL', 'MILLAR');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('MLI', 'METROS LINEALES');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('MT2', 'MTS CUADRADO');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('MT3', 'MTS CUBICOS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('MTS', 'METROS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('NAP', 'NO APLICA');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('PZA', 'PIEZA');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('SRV', 'SERVICIOS');
INSERT INTO unidadmedida (unidad, descunidad) VALUES ('TON', 'TONELADA');

-- centro de costos

INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('ACTIVO', 'ACTIVO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('ADMII', 'ADMINISTRACIÓN II', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('ADMIN', 'ALMACEN ADMINISTRATIVO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AL000', 'ALEN GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AL001', 'VARIOS ALEN', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AL002', 'LINEA DE NITROGENO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AL003', 'REM. OFICINA DIR. PLANEACION', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AL004', 'RAMPA NIVELADORA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AL005', 'RACK PARA TOTES DE TRINCHERA Y CARCAMO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AR000', 'ARA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AT000', 'ATLATEC GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AT001', 'AT001 / SUMINISTRO CONCRETO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('AT002', 'LAGOS DE MORENO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('BC000', 'BARRY CALLEBAUT GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('BO000', 'BOKADOS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('CA000', 'CARSO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('CO000', 'CONGECARIBE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('CPINT', 'CARPINTERIA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('CU000', 'CUPRUM GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('CX000', 'CEMIX GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('DE000', 'DEMEK/AMP', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('EM000', 'EMBRACO GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('EM001', 'MOV. UNIDAD HIDRAULICA ROVETTA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('GENERAL', 'ALMACEN GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('GP000', 'GARZA PONCE GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('GP001', 'SUNGWOO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('GP002', 'ALBAÑILERIA Y ACABADOS NAVE  A  Y  C ', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('ID000', 'INGENERIA Y DISEÑO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('IM000', 'IMSS GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('IS000', 'ICOM STEEL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('JA000', 'AMAYA GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LC000', 'LICITACIONES', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE000', 'LEGO GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE001', 'SALA DE CAPACITACION EMPAQUE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE001-1', 'EXT. SALA DE CAPACITACION EMPAQUE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE002', 'MODIFICACION DE BANQUETAS EN CASETA 1 Y', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE003', 'MEZZANINE MOLDEO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE004', 'SEGUNDA ETAPA ELEVADOR MOLDEO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE005', 'BRICKS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE006', 'MODIFICACION DE ACCESOS CASETA 7', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE007', 'SERVICIO REHABILITACION CARPINTERIA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE008', 'VARIOS LEGO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE009', 'CUARTOS DE LIMPIEZA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE010', 'CONS. CUARTO DE LIMPIEZA 1 EN M2', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE011', 'AREAS DE DESCANSO A1 Y A4', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE012', 'CANOPY EDIFICIO A', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE013', 'CUARTO LIMPIO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE014', 'CONSTRUCCIÓN DE BARDA EN ESCUELA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE015', 'OBRA CIVIL SUB. ELECTRICA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE016', 'CUARTO DE LIMPIEZA 2', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LE017', 'REPARACIONES VILLAMONTAÑA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG000', 'LG GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG001', 'REPARACION DE ANDENES', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG002', 'ELEVADOR', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG004', 'ESTUDIO FOTOGRAFICO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG005', 'DESASOLVE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG006', 'INCREASE IQC IMPORT AREA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG007', 'PUERTAS CORREDIZAS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('LG008', 'LINE TOUR ISSUES BY PRESIDENT', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT000', 'EDIFICIO METROPOLITAN', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT001', 'HVAC COMERCIAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT002', 'TINTORERIA 5ASEC', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT003', 'MURO EXTERIOR O. SPA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT016', 'MOTIVA PISO 16', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT017', 'MOTIVA PISO 17', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT018', 'MOTIVA PISO 18', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT019', 'MOTIVA PISA 19', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT020', 'MOTIVA PISO 20', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT021', 'MOTIVA PISO 21', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT022', 'MOTIVA PISO 22', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT023', 'MOTIVA PISO 23', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT024', 'MOTIVA PISO 24', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT025', 'MOTIVA PISO 25', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT026', 'MOTIVA PISO 26', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MT027', 'MOTIVA PISO 27', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('MZ001', 'CONSTRUCCION MEZZANINE CIVIL REPACK', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM000', 'NEMAK GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM001', 'REM. LAB. DIMENSIONAL CDT', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM002', 'DIST. DE FUERZA DE COMEDOR MAQ. P8', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM003', 'GMET4 LÍNEA DE PREPA.FLLL SW MAST P4', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM004', 'O. CIVIL CUARTO RECIBO OKLAHOMA P6', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM005', 'AUDI Q5 SERV. DE INSTALACION DCM', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM006', 'RACK ESTRUCTUR TOLVAS OKLAHOMAP5', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM007', 'INST. EST. ENDERAZADO - HDPC.', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM008', 'SERV. DCM CCS HPDC', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM009', 'DRENAJE INDUSTRIAL EN PLANTA 12', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM010', 'REACONDICIONAMIENTO LABORATORIO PLANTA 3', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM011', 'PENT.UP SER.INT.AMPLIFICA. LP CIGA 3 P4', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM012', 'SUM E INST SERVICIOS MECANICO P-12 HTT10', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NM013', 'RACK DE SERVICIOS EN PLANTA 12', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO000', 'NOVOCAST GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO001', 'REMODELACION SM', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO002', 'ESTRUCTURA PARA SISTEMA DE ARENAS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO003', 'FABRICACIÓN DE PLATAFORMA EN 3 SECCIONES', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO004', 'SERVICIO DE CIMENTACIONES PARA CRUSHER', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO005', 'NO005 VARIOS MECÁNICOS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO006', 'NO006 OBRA CIVIL CALLE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('NO007', 'CIMENTACION CHIMENEA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PD000', 'PRISMA DESAROLLOS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PD001', 'ESTRUCTURA HTT10 P12', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE000', 'PEPSICO GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE001', 'PEPSICO DF', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE002', 'PEPSICO VILLAHERMOSA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE003', 'PEPSICO CHILPANCINGO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE004', 'OBRA CIVIL TOLUCA & TENANCINGO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE005', 'CEDIS FLETEROS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE006', 'SABRITAS VALLEJO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE007', 'ADECUACION CIP URBANO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PE008', 'CEDIS MONCLOVA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PI001', 'MANTENIMIENTO DE PIIT', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PI002', 'CANCHA MULTIUSOS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PIOOO', 'GOBIERNO DEL ESTADO DE N.L. / PIT', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PK000', 'PENTAKAR', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PV000', 'PAVIA RESICENCIAL TERRACERIAS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PV001', 'PAVIA RESINDENCIAL ESTRUCTURA METALICA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PV002', 'DEPARTAMENTOS PAVIA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PV003', 'NAVE JOSE VELA GUADALUPE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PY000', 'PYROTEK GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PY001', 'VARIOS  2016', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('PÑ000', 'PEÑOLES GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('RG000', 'SIE GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('RS000', 'RESIDENCIAL SAN PATRICIO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('RS001', 'REM. CASA SAN NICOLAS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('RS002', 'REM. CASA SAN JERONIMO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('RS003', 'RS003 / CASA CUMBRES', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('RS004', 'REM. CASA CALLE BRAVO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('SK000', 'SUKARNE GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('TC000', 'TOPO CHICO GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('TL000', 'TALLER ICOM GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('TM000', 'TAMPICO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('TO000', 'TOTO GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('UN000', 'UNIVERSIDAD AUTONOMA DE N.L.', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI000', 'VILLACERO GENERAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI001', 'REMODELACION VILLAUTO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI005', 'TH PESQUERIA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI008', 'REHAU', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI009', 'TUNA FABRICACION DE CISTERNA DE AGUAS RE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI010', 'CINTACERO LAMINACION', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI011', 'TUNA LAMINACION', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI012', 'FUENTES', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI013', 'VILLAGRAN ACOMETIDA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI014', 'PROYECTO EJECUTIVO COMEDOR TORRE VALLE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI015', 'COMEDOR TORRE VALLE', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI016', 'COLOCACIÓN DE CERCA DE MALLA CICLÓNICA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI017', 'L. CARDENAS AIRE AC', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI018', 'TERRENO AEROPUERTO MARIANO ESC.', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI019', 'AMPLIACIÓN ESTACIONAMIENTO VILLAUTO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI020', 'VILLAUTO MERCEDEZ BENZ', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI021', 'CIMENTACION EQUIPO TEMPER', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI022', 'CIMENTACION DE TOTEM VIILAUTO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI023', 'HOTEL BAYMONT LC', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI024', 'OFICINAS MERCEDEZ BENZ', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI025', 'LOCALES EL MARCO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VI026', 'DISEÑO, INGENIERÍAS, VARIOS AEROPUERTO', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VK000', 'INGENIERÍA CIVIL DE MAQUINAS', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VK001', 'PUENTE VEHICULAR Y PEATONAL', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('VK002', 'CISTERNA Y FOZA', '101070001');
INSERT INTO centrocostos (clave, descripcion, cuenta) VALUES ('XI000', 'INSTALACIONES INDUSTRIALES SIE, S.A .DE', '101070001');



INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('ADM', 'ADMIN', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('ALE', 'ALEN', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('AMA', 'AMAYA', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('ARA', 'ARA', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('ATL', 'ATLATEC', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('BAR', 'BARRY CALL', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('BOK', 'BOKADOS', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('CAR', 'CARSO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('CEM', 'CEMIX', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('CON', 'CONGECARIB', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('CUP', 'CUPRUM', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('DEM', 'DEMKE/AMP', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('EMB', 'EMBRACO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('GPO', 'GZA PONCE', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('ICS', 'ICOM STEEL', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('IMS', 'IMSS', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('LEG', 'LEGO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('LG', 'LG', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('LIC', 'LICITACION', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('MAT', 'MATRIZ', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('MDZ', 'MONDELEZ', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('MOT', 'MOTIVA', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('NMK', 'NEMAK', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('NOV', 'NOVOCAST', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PAV', 'PAVIA', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PEP', 'PEPSICO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PEÑ', 'PEÑOLES', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PIT', 'PIT/CIDESI', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PKR', 'PENTAKAR', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PMA', 'PRISMA', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('PYR', 'PYROTEK', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('REM', 'REMISION', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('RES', 'RESIDENCIA', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('SIE', 'SIE', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('SUK', 'SUKARNE', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('TAL', 'TALLE ICOM', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('TAM', 'TAMPICO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('TOP', 'TOPO CHICO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('TOT', 'TOTO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('UNI', 'UANL', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('VIL', 'VILLACERO', 1);
INSERT INTO clientes (idCliente, nombre, regyablq) VALUES ('VKE', 'VIAKABLE', 1);

INSERT INTO `unidadmedida` (`unidad`, `descunidad`) VALUES
('COS', 'COSTAL'),
('CUB', 'CUBETA'),
('DIA', 'DIAS'),
('HRS', 'HORAS'),
('JGO', 'JUEGO'),
('KGS', 'KILOGRAMOS'),
('LOT', 'LOTE'),
('LTS', 'LITROS'),
('MES', 'MESES'),
('MIL', 'MILLAR'),
('MLI', 'METROS LINEALES'),
('MT2', 'MTS CUADRADO'),
('MT3', 'MTS CUBICOS'),
('MTS', 'METROS'),
('NAP', 'NO APLICA'),
('PZA', 'PIEZA'),
('SRV', 'SERVICIOS'),
('TON', 'TONELADA');



